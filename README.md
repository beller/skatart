
To discuss **SKataRT** features and bugs, Use the Forum discussion groups at [https://discussion.forum.ircam.fr/c/skatart](https://discussion.forum.ircam.fr/c/skatart)

## Main Features

### Analysis
- Import of single samples, long recordings, or whole sound directories (recursive)
- Automatic segmentation by time, onset, or silence
- Automatic analysis of 4 audio characteristics
- Detailed corpus editing

### Synthesis
- Synthesis control by X/Y position in timbre space or live input analysis (mosaicing) with calibration
- Gesture-controlled, looped, or beat-synchronous trigger
- Per-grain filter, transposition, gain with random variation
- 8 sub-corpora in voices with independent volume, pitch, pan
- Up to 16 output channels with controllable spatial position and distribution

### Ableton Live Integration
- All parameters automatable
- Total Recall of corpus and all parameters

## Compatibility

* Mac OS >= 10.10, Windows 64bit*
* Live 10 or higher
* Max 8

## Getting Started

Open the Live set in `SKataRT For Live.als`, or the Max project in `SKataRT For Max`.
For details, see the _Quickstart_ document in the distribution, or in the [online documentation](https://support.ircam.fr/docs/skatart/quickstart.html).

*On Windows, you need to have the [Microsoft Visual C++ Redistributable 2015–2019](https://aka.ms/vs/16/release/vc_redist.x64.exe) from [Microsoft Support](https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0) installed to avoid `error 126`.


## Authors

SkataRT is derived from [**CataRT-MuBu**](https://forum.ircam.fr/projects/detail/catart-mubu/) by Diemo Schwarz, the [ISMM team](https://www.stms-lab.fr/team/interaction-son-musique-mouvement), Aaron Einbond, Christopher Trapani, and contributors, and **MIMES** by Olivier Houix, Patrick Susini Nicolas Misdariis ([PDS team](https://www.stms-lab.fr/team/perception-et-design-sonores/)), Frédéric Bevilacqua ([ISMM](https://www.stms-lab.fr/team/interaction-son-musique-mouvement)).
**Max4Live port** by Manuel Poletti, Thomas Goepfer ([IRCAM](http://www.ircam.fr) / [Music Unit](https://forum.ircam.fr/article/detail/musicunit/)), Diemo Schwarz, Riccardo Borghesi ([ISMM team](https://www.stms-lab.fr/team/interaction-son-musique-mouvement)).
**Consulting and quality control** by Jean Lochard ([IRCAM](http://www.ircam.fr)) and Alexis Baskind.
Based on [**MuBu for Max**](https://forum.ircam.fr/projects/detail/mubu/) by Norbert Schnell, Riccardo Borghesi, Diemo Schwarz and the [ISMM team](https://www.stms-lab.fr/team/interaction-son-musique-mouvement).

We acknowledge financial support by the Skat-VG EU project No. 618067 coordinated by [Davide Rochesso](http://skatvg.math.unipa.it) [http://www.skatvg.eu](http://www.skatvg.eu/).
